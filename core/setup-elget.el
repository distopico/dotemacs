;;; Code:
(require 'setup-path)
(add-to-list 'load-path (in-emacs-d "el-get/el-get"))

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (let (el-get-master-branch)
      (goto-char (point-max))
              (eval-print-last-sexp))))

(require 'el-get)

;; local sources
(setq el-get-byte-compile t
      el-get-generate-autoloads t
      el-get-sources

      '(;; UI
        (:name "neotree"
               :description "Neotree Mode"
               :type git
               :url "https://github.com/jaypei/emacs-neotree.git")
        ;; Utils
        (:name "highlight-sexps"
               :description "Highlight sexps"
               :type http
               :url "http://david.rysdam.org/src/emacs/highlight-sexps.el")
        (:name "highlight-parentheses"
               :description "Highlight parentheses"
               :type git
               :url "https://github.com/tsdh/highlight-parentheses.el.git")
        (:name "hideshowvis"
               :description "Hide Show Vis"
               :type git
               :url "https://github.com/sheijk/hideshowvis.git")
        ;; Development
        (:name "jdee-gradle"
               :description "Minor mode for gradle in jdee"
               :type git
               :url "https://github.com/oldButNotWise/jdee-gradle.git")
        (:name "gradle"
               :description "Work in emacs with Gradle build automation tool"
               :type git
               :url "https://github.com/vhallac/gradle-el.git")
        ;; Productive
        (:name "org-reveal"
               :description "reveal.js stuff for orgmode"
               :type git
               :url "https://github.com/yjwen/org-reveal.git")
        ;; Social
        (:name "gnu-social-mode"
               :description "Emacs client for gnu-social"
               :type git
               :url "https://git.savannah.nongnu.org/git/gnu-social-mode.git")
        (:name "jabber-otr"
               :description "jabber.el OTR Plugin"
               :type git
               :url "https://github.com/legoscia/emacs-jabber-otr.git")))

(setq elget-packages
      (append
       '("el-get")
       (mapcar 'el-get-source-name el-get-sources)))

(el-get 'sync elget-packages)

(provide 'setup-elget)
