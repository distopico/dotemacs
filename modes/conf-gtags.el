;;; Code:
(require 'projectile)
(require 'ggtags)

(defcustom distopico:ggtags-update-lighter " Tags-U"
  "Mode-line lighter when `distopico:ggtags-auto-update-mode' is enabled."
  :group 'distopico:ggtags-update
  :type 'string)
1
(defcustom distopico:ggtags-update-delay-seconds  (* 5 60)
  "The delay time until perform ggtags update `after-save-hook'.
Default 5min."
  :type 'integer
  :group 'ctags-update)

(defvar distopico:ggtags-update-last-update-time
  (- (float-time (current-time)) distopico:ggtags-update-delay-seconds 1)
  "Make sure when user first call `distopico:ggtags-update' it can run immediately.")

;; Functions
(defun distopico:projectile-update-tags ()
  "Update the project's gtags."
  (interactive)
  (when (> (- (float-time (current-time))
              distopico:ggtags-update-last-update-time)
           distopico:ggtags-update-delay-seconds)
    (if (and (boundp 'ggtags-mode)
             (memq projectile-tags-backend '(auto ggtags)))
        (progn
          (let* ((ggtags-project-root (projectile-project-root))
                 (default-directory ggtags-project-root))
            (ggtags-ensure-project)
            (ggtags-update-tags t)))
      (let* ((project-root (projectile-project-root))
             (tags-exclude (projectile-tags-exclude-patterns))
             (default-directory project-root)
             (tags-file (expand-file-name projectile-tags-file-name))
             (command (format projectile-tags-command tags-file tags-exclude))
             shell-output exit-code)
        (when (and (file-exists-p tags-file) ;
                   (not (and (buffer-file-name)
                             (string-equal tags-file (buffer-file-name)))) ;
                   (setq distopico:ggtags-update-last-update-time (float-time (current-time)));;update time
                   (with-temp-buffer
                     (setq exit-code
                           (call-process-shell-command command nil (current-buffer))
                           shell-output (projectile-trim-string
                                         (buffer-substring (point-min) (point-max)))))
                   (unless (zerop exit-code)
                     (error shell-output))))))))

;;;###autoload
(define-minor-mode distopico:ggtags-auto-update-mode
  "Auto update TAGS using `gtags' in parent directory."
  :lighter distopico:ggtags-update-lighter
  ;; :global t
  :init-value nil
  :group 'distopico:ggtags-update
  (if distopico:ggtags-auto-update-mode
      (progn
        (add-hook 'after-save-hook 'distopico:projectile-update-tags nil t))
    (remove-hook 'after-save-hook 'distopico:projectile-update-tags t)))

;;;###autoload
(defun distopico:enable-ggtags-mode()
  "Turn on `ggtags-auto-update-mode'."
  (interactive)
  (ggtags-mode +1))

;; Enable for all programming mode
(add-hook 'prog-mode-hook 'distopico:enable-ggtags-mode)


(provide 'conf-gtags)
;;; conf-gtags.el ends here
