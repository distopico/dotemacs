;;; Code:
;; Install extensions if they're missing
(defvar distopico-packages
  '(
    ;; Basic
    async
    dash
    dash-functional ;; require by company-tern
    exec-path-from-shell
    auto-async-byte-compile

    ;; Ido
    crm-custom
    ido-at-point
    ido-complete-space-or-hyphen
    ido-completing-read+
    ido-occur
    ido-select-window
    ido-sort-mtime
    ido-vertical-mode

    ;; Window/Buffer
    ace-window
    windresize
    zoom-window
    buffer-move

    ;; Dired
    dired-hacks-utils
    dired-imenu
    dired-narrow
    dired-subtree

    ;; UI
    anzu
    popwin
    browse-kill-ring
    diminish
    highlight-escape-sequences
    highlight-indent-guides
    highlight-symbol
    hlinum
    linum-off
    visual-fill-column
    message-view-patch
    perspective
    powerline
    rainbow-delimiters
    rainbow-mode
    tabbar
    whitespace-cleanup-mode

    ;; TODO: check those modes/libraries
    iedit
    misc-cmds
    paredit
    smartparens

    ;; Auto Complete
    company-anaconda
    company-quickhelp
    company-restclient
    company-statistics
    company-web
    company

    ;; Development basic
    rg
    aggressive-indent
    counsel-etags
    dumb-jump
    elisp-slime-nav
    ggtags
    magit
    quickrun
    smartscan ;; Test if we should use it
    yasnippet
    git-modes
    multi-term
    tramp-term

    ;; Development tools
    envrc
    android-env ;; I should merge android-mode and android-env in a single package
    android-mode
    dockerfile-mode
    editorconfig
    emr
    eglot
    geiser
    citre
    simple-httpd
    xref-js2
    flymake-elisp-config
    flymake-guile

    ;; Debug
    nodejs-repl
    realgud
    skewer-less
    skewer-mode

    ;; Languages
    anaconda-mode
    gradle-mode
    groovy-mode
    kotlin-mode
    typescript-mode ;; :(
    rust-mode
    haskell-mode
    elm-mode
    geiser-guile
    lua-mode
    php-mode

    ;; Language tools
    java-snippets ;; Move to sub-modules as the other snippets?
    javadoc-lookup
    java-imports
    js-doc
    js2-refactor

    ;; Web
    css-eldoc
    less-css-mode
    web-mode
    rjsx-mode
    js2-mode
    emmet-mode
    yaml-mode
    json-mode
    markdown-mode
    mmm-mode
    jade-mode
    pug-mode ;; Test what is better
    web-beautify
    restclient

    ;; Utilities
    avy
    avy-zap
    htmlize
    hl-todo
    multiple-cursors
    change-inner
    expand-region
    smart-forward
    move-dup
    guix

    ;; Helpers
    goto-last-change
    region-bindings-mode
    which-key

    ;; Projects
    projectile
    org-projectile

    ;; Org / Productivity
    flyspell-correct
    org-contrib
    org-link-types
    org-pomodoro
    org-contacts
    org-notify
    org-present
    org-mime
    toc-org
    ox-rst
    ox-hugo
    ox-gfm

    ;; Activity
    elfeed
    elfeed-org
    jabber
    znc)
  "A list of packages to ensure are installed at launch.")

(provide 'packages)
