;;; Code:
(require 'zoom-window)
(require 'windresize)
(require 'zoom-frm) ;; Make zooming affect frame instead of buffers
(require 'fill-column-indicator)
(require 'highlight-escape-sequences)

;;; Font
(add-to-list 'default-frame-alist
	     '(font . "Hack-10.5"))
(setq-default line-spacing 1
	      font-lock-maximum-decoration t)

;;; Disabled or enabled debug:
(setq debug-on-error nil
      debug-on-signal nil
      debug-on-quit nil)

;; General UI settings
(setq inhibit-startup-screen t
      ;; move window cycle
      windmove-wrap-around t)

;; Don't minimize my emacs!
(setq scroll-bar-mode nil)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; Initial buffer
(setq initial-major-mode 'org-mode
      initial-scratch-message (purecopy "\
# This buffer is for text that is not saved, and for code evaluation.
# To create a file, visit it with C-x C-f and enter text in its buffer."))

;; Smoother scrolling (no multiline jumps.)
(setq scroll-preserve-screen-position t)
;; TODO: verify normal behavior again
      ;; scroll-margin 1
      ;; scroll-step 1
      ;; scroll-conservatively 101
      ;; scroll-up-aggressively 0.01
      ;; scroll-down-aggressively 0.01)

;; hostname and buffer-name in frame title
(setq-default frame-title-format
	      '(:eval (if (string-match-p "^\\*.+\\*$" (buffer-name)) "%b"
		  (format "%s:%s"
			  (or (file-remote-p default-directory 'host)
			      system-name)
			  "%b"))))

;; ;; Default Indentation
;; (setq-default indent-tabs-mode nil
;;               tab-width 4)

;; Set truncate lines enabled b default
(setq-default truncate-lines t
	      fill-column 80)

;; Show indicators for wrapped lines
;; and empty lines
(setq visual-line-fringe-indicators '(t t)
      indicate-buffer-boundaries t
      indicate-empty-lines t)

;; Selection and clipboard behavior
(setq select-active-regions nil
      select-enable-primary t
      select-enable-clipboard t
      ;; not where the mouse cursor is.
      mouse-yank-at-point t)

(delete-selection-mode +1)

;; Highlight
(hes-mode +1) ; escape sequences
(global-hl-line-mode +1) ; line

;; Windows
(setq zoom-window-mode-line-color "black")
(winner-mode +1)

(defalias 'yes-or-no-p 'y-or-n-p)

;; Hooks
(add-hook 'distopico:after-init-load-hook 'distopico:enable--recreate-scratch-buffer)

(provide 'setup-gui)
;;; setup-gui.el ends here
