;;; Code:
(require 'popwin)
(require 'setup-path)

(popwin-mode t)
(global-set-key (kbd "C-c P") popwin:keymap)

;; popwin settings
(setq popwin:special-display-config
      '(("*Help*" :position bottom :height 0.4 :stick t)
        ;; Debug
        ("\\*\\(Warnings\\|Backtrace\\|Messages\\)\\*" :regexp t :position bottom :height 0.3 )
        ("*Compile-Log*" :noselect t :position bottom :stick t)
        ("*Shell Command Output*" :position bottom :height 0.3)
	(".*\\(Debug\\)\\*" :regexp t :position bottom :noselect t :stick nil)
        (".*overtone.log" :regexp t :height 0.3)
        ("collected.org" :position top :height 15)
        (compilation-mode :position bottom :height 0.3 :noselect t)
        ;; Utils
        ("helm" :regexp t :height 0.3)
        ("*Occur*" :position bottom :height 0.3)
        ("\\*Slime Description.*" :regexp t :noselect t :height 0.3)
        ("*undo-tree*" :width 0.3 :position right)
        ("*grep*" :position bottom :height 0.2 :stick t)
        ("*compilation*" :height 0.4 :noselect t :stick t)
        ("\\*\\(ggtags-global\\|quickrun\\)\\*" :regexp t :position bottom :height 0.5 :stick t)
        ;; Magit/vc
        ("*magit-commit*" :position bottom :noselect t :height 0.3 :stick t)
        ("\\*magit.*" :regexp t :position bottom :noselect t :height 0.3 :stick t)
        ("*magit-diff*" :position bottom :noselect t :height 0.3)
        ("*magit-edit-log*" :position bottom :noselect t :height 0.2)
        ("*magit-process*" :position bottom :noselect t :height 0.2)
        ("*vc-diff*" :position bottom :noselect t :height 0.2)
        ("*vc-change-log*" :position bottom :noselect t :height 0.2)
        ;; Navigator
        ("*Ibuffer*" :position bottom :height 0.2)
        ("*Ido Completions*" :noselect t :height 0.3)
        ("*imenu-tree*" :position left :width 50 :stick t)
        ("*Kill Ring*" :height 0.3)
        ("*project-status*" :noselect t)
        (direx:direx-mode :position left :width 50 :dedicated t)
        ;; Programming
        ("*gists*" :height 0.3)
        ("*sldb.*" :regexp t :height 0.3)
        ("*Gofmt Errors*" :noselect t)
        ("\\*godoc*" :regexp t :height 0.3)
        ("*nrepl-error*" :height 0.2 :stick t)
        ("*nrepl-doc*" :height 0.2 :stick t)
        ("*nrepl-src*" :height 0.2 :stick t)
        ("*pytest*" :noselect t)
        ("Django:" :regexp t :width 0.3 :position right)
        ("*Python*" :stick t)
        ("*jedi:doc*" :noselect t)
	("\\*\\(elm-\\).*" :regexp t :position bottom :noselect t :height 0.2 :stick t)
        ;; Console
        ("\\*[\\(ansi-term.*\\|terminal.*\\|shell.*\\)]\\*" :regexp t :height 0.3)
        (term-mode :position :bottom :height 10 :stick t)
        ;; Org/Organized
        (diary-fancy-display-mode :position left :width 50 :stick nil)
        (diary-mode :position bottom :height 15 :stick t)
        (calendar-mode :position bottom :height 15 :stick nil)
        (org-agenda-mode :position bottom :height 15 :stick t)
        ("*Org Agenda.*\\*" :regexp t :position bottom :height 15 :stick t)))

(defun live-display-ansi ()
  (interactive)
  (popwin:display-buffer "*ansi-term*"))

(defun popwin-term:term ()
  (interactive)
  (popwin:display-buffer-1
   (or (get-buffer "*terminal*")
       (save-window-excursion
         (call-interactively 'term)))
   :default-config-keywords '(:position :top)))

(defun popwin-term:ansi-term ()
  (interactive)
  (popwin:display-buffer-1
   (or (get-buffer "*ansi-term*")
       (save-window-excursion
         (ansi-term "/bin/bash")))
   :default-config-keywords '(:position :bottom :height 10 :stick t)))


(defun popwin-term:multi-term ()
  (interactive)
  (popwin:display-buffer-1
   (or (get-buffer "*terminal*")
       (save-window-excursion
         (call-interactively 'multi-term)))
   :default-config-keywords '(:position :bottom :height 10 :stick t)))

(defun popwin-bkr:update-window-reference ()
  (popwin:update-window-reference 'browse-kill-ring-original-window :safe t))

(add-hook 'popwin:after-popup-hook 'popwin-bkr:update-window-reference)

(provide 'conf-popwin)
