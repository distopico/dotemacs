;;; Code:
(require 'python)
(require 'python-environment)
(require 'company)

;; Autoload
(autoload 'anaconda-mode "anaconda" "Anaconda python mode" t)
(autoload 'anaconda-eldoc-mode "anaconda" "Anaconda python mode" t)

;; Default python
(setq python-indent-offset 4
      python-environment-virtualenv '("virtualenv2" "--system-site-packages" "--quiet")
      python-indent-guess-indent-offset nil
      python-environment-directory user-emacs-directory
      python-environment-default-root-name "distopico")

(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))

;; Functions
(defun distopico:python-mode-hook()
  (eldoc-mode +1)
  (anaconda-mode +1)
  (anaconda-eldoc-mode +1)

  (setq indent-tabs-mode nil
        tab-width 4
        auto-indent-assign-indent-level 4)
  (local-set-key (kbd "RET") 'newline-and-indent)

  ;; Autocomplete with company in python
  (add-to-list 'company-backends 'company-anaconda))

;; Hooks
(add-hook 'python-mode-hook 'distopico:python-mode-hook)

(provide 'conf-python)
;;; conf-python-el ends here
