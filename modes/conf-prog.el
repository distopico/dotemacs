;;; Code:
(require 'ispell)
(require 'emr)
(require 'move-dup)
(require 'rainbow-delimiters)
(require 'highlight-symbol)
(require 'whitespace-cleanup-mode)
;; (require 'aggressive-indent)
(require 'citre)
(require 'citre-config)
(require 'rg)
(require 'dumb-jump)

;; Control
(defconst distopico:editorconfig-regexp
  (concat "\\`" (regexp-quote ".editorconfig") "\\'"))

;; Configuration
(setq semanticdb-default-save-directory (in-emacs-d ".cache/semanticdb")
      dumb-jump-selector 'ivy)

;; Exclude some modes fro agressive indent
;; (dolist (source '(diary-mode css-mode less-css-mode jade-mode))
;;   (add-to-list 'aggressive-indent-excluded-modes source t))

;; TODO: Check which-func (which-function-mode 1)

;; Native emacs pair
(electric-pair-mode t)

;; Search files tool
(rg-enable-default-bindings)

;; Define additional/custom keybindings
(define-key prog-mode-map (kbd "C-M-<return>") 'emr-show-refactor-menu)

;; Functions
(defun distopico:local-comment-auto-fill ()
  "Auto-fill just comments."
  (setq-local comment-auto-fill-only-comments t)
  (auto-fill-mode +1))

(defun distopico:prog-mode-hook ()
  "Hook for all modes derivate of `prog-mode'."
  ;; Return and indent
  (local-set-key [(return)] 'newline-and-indent)
  ;; Custom behavior
  (distopico:local-comment-auto-fill)
  ;; Enable highlight `TODO' comments
  (hl-todo-mode +1)
  ;; Automatic symbol highlighting
  (highlight-symbol-mode +1)
  ;; delimiters highlighting
  (rainbow-delimiters-mode +1)
  ;; Enable docs
  (eldoc-mode +1)
  ;; Moving and duplicating lines or rectangles
  (move-dup-mode +1)
  ;; Search references
  (dumb-jump-mode +1)
  ;; Emacs refactor
  (emr-initialize)
  ;; If editorconfig not found clean but only
  ;; if the whitespace in the buffer was initially clean
  (unless (distopico:locate-parent-file distopico:editorconfig-regexp)
    (whitespace-cleanup-mode +1))
  ;; Enable spell check on comments
  (when (executable-find ispell-program-name)
    (flyspell-prog-mode)))

;; Hooks
(add-hook 'prog-mode-hook #'distopico:prog-mode-hook)
(add-hook 'prog-mode-hook 'display-fill-column-indicator-mode)

(provide 'conf-prog)
;;; conf-prog.el ends here
