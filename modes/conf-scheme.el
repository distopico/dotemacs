;;; Code:
(require 'geiser)
(require 'geiser-mode)
(require 'geiser-guile)
(require 'paredit)
(require 'rainbow-delimiters)
(require 'flymake-guile)

(setq geiser-mode-start-repl-p t
      ;; geiser-guile-load-init-file t
      geiser-default-implementation 'guile
      geiser-active-implementations '(guile))

(defun distopico:fix-compile-goto-error ()
"Fixes compile go to error adding guile load path."
  (let* ((shell-command "guile -c '(write %load-path)'")
	 (shell-output (shell-command-to-string shell-command))
	 (load-paths (car (read-from-string shell-output))))
    (dolist (path load-paths)
      (add-to-list 'compilation-search-path path))))

(defun distopico:scheme-mode-hook ()
 "Scheme mode hook to enhance it."
 (enable-paredit-mode)
 (flymake-guile))

(with-eval-after-load 'geiser-guile
  (require 'compile)
  (distopico:fix-compile-goto-error))

;; hooks
(add-hook 'scheme-mode-hook #'distopico:scheme-mode-hook)

(provide 'conf-scheme)
;;; conf-scheme.el ends here
