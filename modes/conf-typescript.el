;;; Code:
(require 'web-mode)

(defun distopico:typescript-mode-hook ()
  "Hook to prepare `typescrip-mode'."
  ;; Add node_modules to exec path
  (distopico:add-node-modules-path)
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)

  ;; Add company backend for TS
  (set (make-local-variable 'company-backends)
       '(company-bbdb
         company-nxml company-css
         company-semantic company-files
         (company-dabbrev-code company-gtags company-etags company-keywords company-tern :with company-yasnippet)
         (company-dabbrev company-capf company-keywords :with company-yasnippet)))

  ;; Add this hook locally
  (add-hook 'before-save-hook 'tide-format-before-save nil 'make-it-local))

(defun distopico:web-mode-hook ()
  (when (string-equal "tsx" (file-name-extension buffer-file-name))
    (distopico:typescript-mode-hook)))

;; TSX
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))

;; Hooks
(add-hook 'web-mode-hook #'distopico:web-mode-hook)
(add-hook 'typescript-mode-hook #'distopico:typescript-mode-hook)
;;; conf-typescript.el ends here
