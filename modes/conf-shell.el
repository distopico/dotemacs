;;; Code:
(setq shell-file-name "bash"
      eshell-cmpl-cycle-completions nil)

;; eshell scrolling betterness
(defun distopico:eshell-scroll-to-bottom (window display-start)
  (if (and window (window-live-p window))
      (let ((resize-mini-windows nil))
	(save-selected-window
	  (select-window window)
	  (save-restriction
	    (widen)
	    (when (> (point) eshell-last-output-start) ; we're editing a line. Scroll.
	      (save-excursion
		(recenter -1)
		(sit-for 0))))))))

(defun eshell-add-scroll-to-bottom ()
  (interactive)
  ;(make-local-hook 'window-scroll-functions)
  (add-hook 'window-scroll-functions 'eshell-scroll-to-bottom nil t))

(defun distopico:eshell-unset-pager ()
  (setenv "PAGER" ""))

;; Hooks
(add-hook 'eshell-mode-hook 'distopico:eshell-add-scroll-to-bottom)
(add-hook 'eshell-mode-hook 'distopico:eshell-unset-pager)
;; Color shell text
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

(provide 'conf-shell)
;;; conf-shell.el ends here
