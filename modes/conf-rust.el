;;; Code:
(require 'rust-mode)
(require 'ggtags)
(require 'eglot)

(defun distopico:rust-mode-hook()
  "The `rust-mode' hook."
  (ggtags-mode 1)
  (eglot-ensure))

(defun distopico:try-cargo-toml(dir)
  "Find rust workspace root for `project' based in the current `DIR'."
  (when-let* (((eq major-mode 'rust-mode))
              (output
               (let ((default-directory dir))
                 (shell-command-to-string "cargo metadata --no-deps --format-version 1")))
              (json (ignore-errors (json-read-from-string output)))
              (found (cdr (assq 'workspace_root json))))
    (cons 'transient  found)))

(defun distopico:eglot--managed-mode-hook ()
  "Hooks for eglot managed-mode to disable `eglot-inlay-hints-mode'."
  ;; Too noise for me
  (eglot-inlay-hints-mode -1))

;; Hooks
(add-hook 'rust-mode-hook #'distopico:rust-mode-hook)
(add-hook 'project-find-functions #'distopico:try-cargo-toml)
(add-hook 'eglot-managed-mode-hook #'distopico:eglot--managed-mode-hook)

(provide 'conf-rust)
;;; conf-rust.el ends here
