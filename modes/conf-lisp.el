;;; Code:
(require 'flymake-elisp-config)

(setq flymake-elisp-config-config-file-name-regexp-list
      (list "\\(?:\\.emacs\\.d/\\(.*\\)\\.el\\|init\\.el\\)$"))

(flymake-elisp-config-global-mode)
(flymake-elisp-config-auto-mode)

(provide 'conf-lisp)
;;; conf-lisp.el ends here
